FROM node:16

RUN npm i -g @nestjs/cli

WORKDIR /app

CMD yarn && yarn run start:debug --preserveWatchOutput